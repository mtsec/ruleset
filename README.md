RuleSet

Lưu ý: Nếu cài trên SDK đứng sau LB thì cần xóa bỏ nội dung của các file:

- 09_Bruteforce_Bruteforce.conf
- 11_HTTP_HTTPDoS.conf


Đây là 2 file hạn chế ddos nếu SDK đứng sau LB thì user sẽ bị chặn do trên Modsec không thể nhận dạng được IP của người dùng mà mặc định sẽ nhận dạng IP của LB nên sẽ đánh dấu LB là IP đang tiến hành tấn công hệ thống
